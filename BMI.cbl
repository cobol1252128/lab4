       IDENTIFICATION DIVISION.
       PROGRAM-ID. BMI.
       AUTHOR. SUPHAKORN.

       DATA   DIVISION.
       WORKING-STORAGE SECTION.
       01  WEIGHT  PIC 9(3)V99.
       01  HEIGHT  PIC 9(3)V99.
       01  BMI     PIC 9(3)V99.
       01  BMI-OUTPUT  PIC 9(3)V99.

       PROCEDURE DIVISION.
       BEGIN.
           DISPLAY "Enter Weight(kg) : " WITH NO ADVANCING
           ACCEPT WEIGHT
           DISPLAY "Enter Height(m) : " WITH NO ADVANCING
           ACCEPT HEIGHT
           COMPUTE BMI = (WEIGHT / (HEIGHT * HEIGHT)). 
           MOVE BMI TO BMI-OUTPUT
           DISPLAY "Your BMI is " BMI-OUTPUT
           . 
