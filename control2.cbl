       IDENTIFICATION DIVISION.
       PROGRAM-ID. CONTROL2.
       AUTHOR. SUPHAKORN.

       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
       SPECIAL-NAMES.
           CLASS    HEX-NUMBER IS "0" THRU "9", "A" THRU "F"
           CLASS    REAL-NAME IS "A"  THRU "Z",   "a" THRU "z", SPACE.


       DATA DIVISION.
       WORKING-STORAGE SECTION.
       01  NUM-IN   PIC X(4).
       01  NAME-IN  PIC X(15).

       PROCEDURE DIVISION.
       BEGIN.
           DISPLAY "Enter a Hex number - " WITH NO ADVANCING
           ACCEPT   NUM-IN
           IF NUM-IN IS HEX-NUMBER THEN
              DISPLAY  NUM-IN   " Is a Hex number."
           ELSE
              DISPLAY  NUM-IN   " Is not a Hex number."
           END-IF
           DISPLAY "Enter a Real name - " WITH NO ADVANCING
           ACCEPT   NAME-IN
           IF NAME-IN IS REAL-NAME THEN
              DISPLAY  NAME-IN   " Is a Real name."
           ELSE
              DISPLAY  NAME-IN   " Is not a Real name."
           END-IF

           DISPLAY "Enter a Name - " WITH NO ADVANCING
           ACCEPT   NAME-IN
           IF NAME-IN IS ALPHABETIC THEN
              DISPLAY  NAME-IN   " Is a Name."
           ELSE
              DISPLAY  NAME-IN   " Is not a Name."
           END-IF 
           . 
       